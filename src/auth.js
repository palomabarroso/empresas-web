export const auth = () =>{
    if (localStorage.getItem('ioasys_token') !== '' && localStorage.getItem('ioasys_client') && localStorage.getItem('ioasys_uid')){
        return true;
    }
    return false;
}