import React from 'react';
import { BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';
import { auth } from './auth';
import Login from './Components/Login/Login';
import HomeBusca from './Components/HomeBusca/HomeBusca';

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        auth() ? (<Component {...props} />) :
            (<Redirect to={{ pathname: "/", state: { from: props.location } }} />)
    )} />
);

export default function Routes() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Login} />
                <PrivateRoute path="/empresas" component={HomeBusca} />
            </Switch>
        </BrowserRouter>
    )
}