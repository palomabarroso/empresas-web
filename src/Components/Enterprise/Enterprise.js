import React, { Component } from 'react';
import './Enterprise.css';
import ImgEnterprise from '../../img/lista.png';

class Card extends Component {
    render() {
        const {
            enterprise_name,
            country,
            enterprise_type,
            photo
        } = this.props.cardItem;

        return (
            <div className='cardItem' >
                <div className='cardPhoto'>
                    <img src={ImgEnterprise} alt='' />
                    
                </div>
                <div className='cardDetails'>
                    <h1>{enterprise_name}</h1>
                    <h2>{country}</h2>
                    <h3>{enterprise_type.enterprise_type_name}</h3>
                </div>
            </div>
        )
    }
}
export default Card;