import React, { Component } from 'react';
import axios from 'axios';
import './HomeBusca.css';
import Enterprise from '../Enterprise/Enterprise';
import iconSearch from '../../img/ic-search.png';

class HomeBusca extends Component {
    state = {
        inputValue: '',
        enterprises: [],
        resultApi: []
    }

    onChangeSearch = async (e) => {
        await this.setState({
            inputValue: (e.target.value.toLowerCase())
        })

        const { inputValue } = this.state;
        axios({
            method: 'get',
            url: 'http://empresas.ioasys.com.br/api/v1/enterprises',
            headers: {
                ['access-token']: localStorage.getItem('ioasys_token'),
                ['client']: localStorage.getItem('ioasys_client'),
                ['uid']: localStorage.getItem('ioasys_uid')
            },
            params: {
                name: inputValue
            }
        })
            .then(response => {
                if (response.status === 200) {
                    this.setState({
                        enterprises: response.data.enterprises,
                    });
                }
            });
    }

    render() {
        return (
            <div className='container '>
                <header className='header'>
                    <div className='pesquisa inputIcon'>
                        <input type='search' className='' onChange={this.onChangeSearch} placeholder='Pesquisa' />
                        <i><img src={iconSearch} alt='E-mail'></img></i>
                    </div>
                </header>
                <div className='cards'>
                    {this.state.enterprises.map((cardItem) => (
                        <Enterprise key={cardItem.id} cardItem={cardItem} onClick={this.detailEnterprise}/>
                    ))}
                </div>
            </div>
        )
    }
}
export default HomeBusca;