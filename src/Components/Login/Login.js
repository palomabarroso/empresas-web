import React, { Component } from 'react';
import axios from 'axios';
import loginImg from '../../img/logo-home.png';
import iconEmail from '../../img/ic-email.png';
import iconCadeado from '../../img/ic-cadeado.png';
import './Login.css';
import {Redirect} from 'react-router-dom';
import {auth} from '../../auth';

class Login extends Component {
    state = {
        email: '',
        password: '',
        token: '',
        uid: '',
        client: '',
        redirect:auth()
    }

    handleLogin = async (e) => {
        e.preventDefault();
        axios({
            method: 'post',
            url: 'http://empresas.ioasys.com.br/api/v1/users/auth/sign_in',
            data: {
                email: this.state.email,
                password: this.state.password
            }
        })
            .then(response => {
                if (response.status === 200) {
                    this.setState({
                        token:response.headers['access-token'],
                        client: response.headers['client'],
                        uid: response.headers['uid'],
                        redirect:true
                    });

                    localStorage.setItem('ioasys_token',this.state.token);
                    localStorage.setItem('ioasys_client',this.state.client);
                    localStorage.setItem('ioasys_uid',this.state.uid);
                }
            });
    }

    render() {
        
        if (this.state.redirect) {
            return <Redirect to='/empresas'/>;
        }
        
        return (
            <div className='Login' >
                <div className='logoImg' >
                    <img src={loginImg} alt='Logo' />
                </div>

                <div>
                    <p className='titulo'>BEM-VINDO AO<br /> EMPRESAS</p>
                </div>

                <p className='subtitulo'>
                    Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
                </p>

                <form onSubmit={this.handleLogin}>
                    <div className='inputIcon'>
                        <i><img src={iconEmail} alt='E-mail'></img></i>
                        <input
                            name='email'
                            type='text'
                            placeholder='E-mail'
                            onChange={e => this.setState({ email: e.target.value })}
                            requireds
                        />
                    </div>
                    <div className='inputIcon'>
                        <i><img src={iconCadeado} alt='Senha'></img></i>
                        <input
                            name='password'
                            type='password'
                            placeholder='Senha'
                            onChange={e => this.setState({ password: e.target.value })}
                            required
                        />
                    </div>
                    <button type='submit' className='button-large'>ENTRAR</button>
                </form>
            </div>
        )
    }
}

export default Login;